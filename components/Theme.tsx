import createMuiTheme from '@material-ui/core/styles/createMuiTheme';
// import useMediaQuery from '@material-ui/core/useMediaQuery';
import ThemeProvider from '@material-ui/styles/ThemeProvider';
import { useMemo } from 'react';

export const AppThemeProvider: React.FC = ({ children }) => {
  // const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)');
  const theme = useMemo(
    () =>
      createMuiTheme({
        typography: {
          fontFamily: "'Rawline Regular', sans-serif",
        },
      }),
    [],
  );
  return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
};
