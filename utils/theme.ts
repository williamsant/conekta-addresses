import createMuiTheme from '@material-ui/core/styles/createMuiTheme';

const theme = createMuiTheme({
  typography: {
    fontFamily: "'Rawline Regular', sans-serif",
  },
});

export default theme;
