export interface Place {
  fullAddress: string;
  name: string;
  placeId: string;
  latitude: number;
  longitude: number;
  route: string;
  streetInfo: {
    internal: string | null;
    external: string | null;
    hasSharp: boolean;
  };
}
