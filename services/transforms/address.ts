import type { AddressComponent } from '@googlemaps/google-maps-services-js';
import {
  AddressType,
  GeocodingAddressComponentType,
} from '@googlemaps/google-maps-services-js';

export const transformFullAddress = (
  components: AddressComponent[],
): string => {
  const { locality, country, principalArea, subLocality } = components.reduce(
    (reduced, currentComponent) => {
      if (currentComponent.types.includes(AddressType.country)) {
        reduced.country = currentComponent.long_name;
      } else if (
        currentComponent.types.includes(AddressType.administrative_area_level_1)
      ) {
        reduced.principalArea = currentComponent.long_name;
      } else if (
        currentComponent.types.includes(AddressType.administrative_area_level_2)
      ) {
        reduced.locality = currentComponent.long_name;
      } else if (
        currentComponent.types.includes(AddressType.sublocality_level_1)
      ) {
        reduced.subLocality = currentComponent.long_name;
      }
      return reduced;
    },
    {
      locality: null,
      subLocality: null,
      country: null,
      principalArea: null,
    },
  );
  return `${locality || subLocality}, ${principalArea}, ${country}`;
};

export const transformDetailAddress = (components: AddressComponent[]) => {
  const detail = components.reduce(
    (reduced, currentComponent) => {
      if (currentComponent.types.includes(AddressType.route)) {
        reduced.route = currentComponent.long_name;
      } else if (
        currentComponent.types.includes(
          GeocodingAddressComponentType.street_number,
        )
      ) {
        reduced.streetInfo.hasSharp = currentComponent.long_name.includes('#');
        const noSharp = currentComponent.long_name.replace('#', '');
        const [internal, external] = noSharp.split('-');
        reduced.streetInfo.internal = internal || null;
        reduced.streetInfo.external = external || null;
      }
      return reduced;
    },
    {
      route: null,
      streetInfo: { internal: null, external: null, hasSharp: false },
    },
  );
  detail.streetInfo.internal = detail.streetInfo.internal || '00';
  return detail;
};
