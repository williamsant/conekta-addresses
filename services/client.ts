import { Client as MapsClient } from '@googlemaps/google-maps-services-js';

export default class Client {
  private static _instance: MapsClient | null = null;

  public static getInstance(): MapsClient {
    if (!Client._instance) {
      Client._instance = new MapsClient({});
    }
    return Client._instance;
  }
}
