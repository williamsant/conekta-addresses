import Client from 'services/client';
import {
  AddressType,
  PlaceDetailsRequest,
  ReverseGeocodeRequest,
  TextSearchRequest,
} from '@googlemaps/google-maps-services-js';

import {
  transformDetailAddress,
  transformFullAddress,
} from 'services/transforms/address';

const getRawPlaceDetails = async (placeId: string) => {
  const client = Client.getInstance();
  const findRequest: PlaceDetailsRequest = {
    params: {
      place_id: placeId,
      key: process.env.GOOGLE_MAPS_API_KEY,
    },
  };
  const {
    data: { result },
  } = await client.placeDetails(findRequest);
  return result;
};

export const getPlaceDetails = async (placeId: string) => {
  try {
    const details = await getRawPlaceDetails(placeId);
    const fullAddress = transformFullAddress(details.address_components);

    return {
      fullAddress,
      name: details.name,
      placeId,
      latitude: details.geometry.location.lat,
      longitude: details.geometry.location.lng,
      ...transformDetailAddress(details.address_components),
    };
  } catch (err) {
    return null;
  }
};

export const queryPLaces = async (textQuery: string, location: string) => {
  const client = Client.getInstance();
  const findRequest: TextSearchRequest = {
    params: {
      query: textQuery,
      location,
      radius: 10000,
      key: process.env.GOOGLE_MAPS_API_KEY,
    },
  };
  const {
    data: { results },
  } = await client.textSearch(findRequest);
  const promises = results.map(async ({ place_id: placeId, name }) => {
    const details = await getRawPlaceDetails(placeId);
    const fullAddress = transformFullAddress(details.address_components);

    return {
      fullAddress,
      name,
      placeId,
      details,
    };
  });

  const addresses = await Promise.all(promises);
  return addresses
    .filter((address) =>
      address.details.address_components.some((component) =>
        component.types.includes(AddressType.route),
      ),
    )
    .map(({ fullAddress, name, placeId }) => ({ fullAddress, name, placeId }));
};

export const getAddressFromLocation = async (location: string) => {
  const client = Client.getInstance();
  const findRequest: ReverseGeocodeRequest = {
    params: {
      latlng: location,
      key: process.env.GOOGLE_MAPS_API_KEY,
    },
  };
  const {
    data: { results },
  } = await client.reverseGeocode(findRequest);
  const accurate = results[0];
  if (!accurate) {
    return null;
  }
  const { place_id: placeId, geometry } = accurate;
  const details = await getRawPlaceDetails(placeId);
  const fullAddress = transformFullAddress(details.address_components);

  return {
    fullAddress,
    name: details.name,
    placeId,
    latitude: geometry.location.lat,
    longitude: geometry.location.lng,
    ...transformDetailAddress(details.address_components),
  };
};
