import createStyles from '@material-ui/core/styles/createStyles';
import withStyles from '@material-ui/core/styles/withStyles';
import InputBase from '@material-ui/core/InputBase';
import type { Theme } from '@material-ui/core';

export const DescriptionInput = withStyles((theme: Theme) =>
  createStyles({
    root: {
      'label + &': {
        marginTop: theme.spacing(3),
      },
      width: '100%',
      backgroundColor: theme.palette.common.white,
      borderBottom: '1px solid #B9B9B9',
    },
    input: {
      position: 'relative',
      border: '0',
      fontSize: 18,
      width: '100%',
      height: '26px',
      letterSpacing: '0.005em',
      padding: '10px 12px',
      lineHeight: '30px',
      // Use the system font instead of the default Roboto font.
      fontFamily: ['-apple-system', '"Rawline SemiBold"', 'sans-serif'].join(
        ',',
      ),
    },
  }),
)(InputBase);
