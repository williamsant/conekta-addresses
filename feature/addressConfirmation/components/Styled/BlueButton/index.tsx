import withStyles from '@material-ui/core/styles/withStyles';
import Button from '@material-ui/core/Button';
import type { Theme } from '@material-ui/core';

export const BlueButton = withStyles((theme: Theme) => ({
  root: {
    padding: '16px 11px',
    color: theme.palette.getContrastText('#131D39'),
    fontFamily: 'Rawline Bold',
    borderRadius: '11px',
    boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
    backgroundColor: '#131D39',
    '&:hover': {
      backgroundColor: '#0A132C',
    },
  },
}))(Button);
