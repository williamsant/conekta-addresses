import Image from 'next/image';
import Typography from '@material-ui/core/Typography';
import { IoChevronBack } from 'react-icons/io5';

export const ConfirmHeader = () => {
  return (
    <div style={{ marginTop: '16px', padding: '0 16px' }}>
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <Image src="/conekta.png" width={34} height={37} />
        <Typography
          variant="h2"
          style={{
            marginLeft: '18px',
            fontSize: '24px',
            fontFamily: 'Rawline Bold',
            letterSpacing: '0.005em',
          }}
        >
          Confirmar dirección
        </Typography>
      </div>
      {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events */}
      <div
        id="saveChanges"
        tabIndex={0}
        onClick={() => window.location.assign('/')}
        role="button"
        aria-pressed="false"
        style={{
          cursor: 'pointer',
          marginLeft: '-15px',
          marginTop: '10px',
          marginBottom: '10px',
          width: '80px',
          display: 'flex',
          alignItems: 'center',
        }}
      >
        <IoChevronBack size={24} />
        <Typography
          variant="body1"
          style={{
            fontFamily: 'Rawline SemiBold',
            letterSpacing: '0.005em',
          }}
        >
          Atrás
        </Typography>
      </div>
    </div>
  );
};
