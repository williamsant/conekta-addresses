// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck

import { Map, Marker, GoogleApiWrapper } from 'google-maps-react';
import type { FC } from 'react';
import { useEffect, useMemo, useRef } from 'react';

const MapContainer = ({ google, latitude: lat, longitude: lng }) => {
  const mapRef = useRef(null);

  useEffect(() => {
    if (mapRef.current) {
      const mapDiv: HTMLDivElement = mapRef.current.mapRef.current;
      mapDiv.parentNode.style.height = null;
      mapDiv.parentNode.style.zIndex = 0;
    }
  }, [mapRef]);
  return useMemo(
    () => (
      <Map
        ref={mapRef}
        style={{ width: '100%', height: '245px' }}
        google={google}
        initialCenter={{
          lat,
          lng,
        }}
        zoom={18}
        scrollwheel={false}
        draggable={false}
        keyboardShortcuts={false}
        disableDoubleClickZoom
        zoomControl={false}
        mapTypeControl={false}
        scaleControl={false}
        streetViewControl={false}
        panControl={false}
        rotateControl={false}
        fullscreenControl={false}
      >
        <Marker
          position={{
            lat,
            lng,
          }}
          name="Current location"
        />
      </Map>
    ),
    [google, lat, lng],
  );
};

export const MapMarker: FC<{
  apiKey: string;
  latitude: number;
  longitude: number;
}> = GoogleApiWrapper((props) => ({
  apiKey: props.apiKey,
}))(MapContainer);
