import ListItemText from '@material-ui/core/ListItemText';
import { FaLocationArrow } from 'react-icons/fa';
import Avatar from '@material-ui/core/Avatar';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import { MdMyLocation } from 'react-icons/md';
import Divider from '@material-ui/core/Divider';

export const CurrentAddressOptions = ({ address, onClick }) => {
  return (
    <>
      {address && (
        <>
          <ListItem button onClick={() => onClick(address.placeId)}>
            <ListItemAvatar>
              <Avatar style={{ color: '#fff', backgroundColor: '#5ca4a9' }}>
                <FaLocationArrow size={16} />
              </Avatar>
            </ListItemAvatar>
            <ListItemText
              primaryTypographyProps={{
                style: { fontFamily: 'Rawline Bold' },
              }}
              primary={address.name}
              secondary={address.fullAddress}
            />
            <ListItemSecondaryAction>
              <MdMyLocation size={24} />
            </ListItemSecondaryAction>
          </ListItem>
          <Divider component="li" />
        </>
      )}
    </>
  );
};
