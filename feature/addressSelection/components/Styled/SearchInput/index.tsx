import createStyles from '@material-ui/core/styles/createStyles';
import withStyles from '@material-ui/core/styles/withStyles';
import type { Theme } from '@material-ui/core';
import InputBase from '@material-ui/core/InputBase';

export const SearchInput = withStyles((theme: Theme) =>
  createStyles({
    root: {
      'label + &': {
        marginTop: theme.spacing(3),
      },
      width: '100%',
      borderRadius: 6,
      boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
      backgroundColor: theme.palette.common.white,
    },
    input: {
      position: 'relative',
      border: '0',
      fontSize: 16,
      width: '100%',
      height: '26px',
      padding: '10px 12px',
      // Use the system font instead of the default Roboto font.
      fontFamily: ['-apple-system', '"Rawline Regular"', 'sans-serif'].join(
        ',',
      ),
    },
  }),
)(InputBase);
