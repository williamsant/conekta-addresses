import { useEffect, useRef, useState, Fragment } from 'react';
import { Place } from 'models/place';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import { fetchAddresses } from 'feature/addressSelection/helpers/fetchAddresses';

export const AddressSearchOptions = ({
  search,
  lat,
  lng,
  onClick,
  serverAddresses,
}) => {
  const [addresses, setAddresses] = useState<Place[]>(serverAddresses);
  const refInitialRender = useRef(false);
  useEffect(() => {
    if (!refInitialRender.current && serverAddresses.length) {
      refInitialRender.current = true;
      return;
    }
    if (!search || !lat || !lng) {
      setAddresses([]);
      return;
    }
    fetchAddresses(search, lat, lng).then(setAddresses);
  }, [lat, lng, search, serverAddresses.length]);
  return (
    <>
      {addresses.map((address) => (
        <Fragment key={address.placeId}>
          <ListItem button onClick={() => onClick(address.placeId)}>
            <ListItemText
              primaryTypographyProps={{
                style: { fontFamily: 'Rawline Bold' },
              }}
              primary={address.name}
              secondary={address.fullAddress}
            />
          </ListItem>
          <Divider component="li" />
        </Fragment>
      ))}
    </>
  );
};
