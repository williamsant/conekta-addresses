import Image from 'next/image';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import { GrLocation } from 'react-icons/gr';
import { RiCloseCircleFill } from 'react-icons/ri';
import { useEffect, useRef, useState } from 'react';
import debounce from 'lodash.debounce';
import { SearchInput } from 'feature/addressSelection/components/Styled/SearchInput';

export const SearchHeader = ({ onInputChange, serverSearch }) => {
  const [localSearch, setLocalSearch] = useState(serverSearch || '');

  const inputChangeDebounced = useRef(debounce(onInputChange, 1000));

  useEffect(() => {
    inputChangeDebounced.current(localSearch || null);
  }, [inputChangeDebounced, localSearch]);
  return (
    <div
      style={{
        position: 'fixed',
        zIndex: 1000,
        backgroundColor: '#f3f3f3',
        height: '156px',
        width: '100%',
        top: 0,
        left: 0,
      }}
    >
      <div style={{ marginTop: '16px', padding: '0 18px' }}>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <Image src="/conekta.png" width={34} height={37} />
          <Typography
            variant="h2"
            style={{
              marginLeft: '18px',
              fontSize: '24px',
              fontFamily: 'Rawline Bold',
              letterSpacing: '0.005em',
            }}
          >
            Datos de ubicación
          </Typography>
        </div>
        <SearchInput
          placeholder="Ingresa una dirección"
          value={localSearch}
          onChange={(evt) => setLocalSearch(evt.target.value)}
          style={{ marginTop: '19px' }}
          startAdornment={
            <InputAdornment style={{ marginLeft: '8px' }} position="start">
              <GrLocation size={25} />
            </InputAdornment>
          }
          endAdornment={
            <InputAdornment style={{ marginRight: '8px' }} position="end">
              <IconButton
                style={{ color: '#000', padding: '0 8px 0 0' }}
                component="span"
                onClick={() => setLocalSearch('')}
              >
                <RiCloseCircleFill size={25} />
              </IconButton>
            </InputAdornment>
          }
        />
        <div style={{ marginLeft: '5px', marginTop: '5px' }}>
          <Image src="/Google-Logo.png" width={54} height={30} />
        </div>
      </div>
    </div>
  );
};
