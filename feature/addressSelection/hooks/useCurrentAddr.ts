import { useEffect, useState } from 'react';
import { Place } from 'models/place';
import { getGeoLocation } from 'feature/addressSelection/helpers/getGeolocation';
import { fetchLocation } from 'feature/addressSelection/helpers/fetchLocation';

export const useCurrentAddr = () => {
  const [coords, setCoords] = useState<GeolocationCoordinates | null>(null);
  const [currentAddress, setCurrentAddress] = useState<Place>(null);

  useEffect(() => {
    getGeoLocation()
      .then(({ coords: newCoords }) => setCoords(newCoords))
      .catch((err) => console.warn(`ERROR(${err.code}): ${err.message}`));
  }, []);

  useEffect(() => {
    if (!coords) return;
    fetchLocation(coords).then(setCurrentAddress);
  }, [coords]);

  return { currentAddress, coords };
};
