export const fetchLocation = ({
  latitude,
  longitude,
}: GeolocationCoordinates) => {
  return fetch(`/api/location?coords=${latitude},${longitude}`).then((res) =>
    res.json(),
  );
};
