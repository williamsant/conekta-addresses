import { Place } from 'models/place';

export const fetchAddresses = (
  query: string,
  lat: string,
  lng: string,
): Promise<Place[]> => {
  return fetch(
    `/api/places?coords=${lat},${lng}&query=${encodeURIComponent(query)}`,
  ).then((res) => res.json());
};
