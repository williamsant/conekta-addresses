export const getGeoLocation = (
  options = {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 0,
  },
): Promise<GeolocationPosition> => {
  return new Promise((resolve, reject) =>
    navigator.geolocation.getCurrentPosition(resolve, reject, options),
  );
};
