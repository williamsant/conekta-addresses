import Container from '@material-ui/core/Container';
import { SearchHeader } from 'feature/addressSelection/components/SearchHeader';
import { CurrentAddressOptions } from 'feature/addressSelection/components/CurrentAddressOptions';
import { useState } from 'react';
import List from '@material-ui/core/List';
import { useCurrentAddr } from 'feature/addressSelection/hooks/useCurrentAddr';
import { AddressSearchOptions } from 'feature/addressSelection/components/AddressSearchOptions';
import { queryPLaces } from 'services/search';

export default function Home({ serverAddresses, initialQuery }) {
  const [search, setSearch] = useState<string | null>(initialQuery || null);
  const { currentAddress, coords } = useCurrentAddr();
  const { longitude, latitude } = coords || { latitude: null, longitude: null };
  const navigateConfirm = (placeId: string) =>
    window.location.assign(`/confirm/${placeId}`);

  return (
    <Container style={{ padding: 0 }}>
      <SearchHeader onInputChange={setSearch} serverSearch={initialQuery} />
      <div style={{ marginTop: '156px', backgroundColor: '#fff' }}>
        <List component="nav" aria-label="survey list" style={{ padding: 0 }}>
          <AddressSearchOptions
            lat={latitude}
            lng={longitude}
            search={search}
            serverAddresses={serverAddresses}
            onClick={navigateConfirm}
          />
          <CurrentAddressOptions
            address={currentAddress}
            onClick={navigateConfirm}
          />
        </List>
      </div>
    </Container>
  );
}

export const getServerSideProps = async (ctx) => {
  const { initial_query: initialQuery, coords } = ctx.query;
  let placeData = [];
  if (initialQuery && coords) {
    placeData = await queryPLaces(String(initialQuery), String(coords));
  }
  return {
    props: { serverAddresses: placeData, initialQuery: initialQuery || null },
  };
};
