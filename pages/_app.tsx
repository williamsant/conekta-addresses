import '../styles/globals.scss';
import { AppThemeProvider } from 'components/Theme';
import CssBaseline from '@material-ui/core/CssBaseline';
import { useEffect } from 'react';
import { SnackbarProvider } from 'notistack';

function MyApp({ Component, pageProps }) {
  useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  return (
    <AppThemeProvider>
      <SnackbarProvider
        maxSnack={3}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        preventDuplicate
      >
        <CssBaseline />
        <Component {...pageProps} />
      </SnackbarProvider>
    </AppThemeProvider>
  );
}

export default MyApp;
