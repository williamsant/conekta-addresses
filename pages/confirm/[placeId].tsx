import { useState } from 'react';
import { MapMarker } from 'feature/addressConfirmation/components/MapMarker';
import { getPlaceDetails } from 'services/search';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import { MdCreate } from 'react-icons/md';
import { ConfirmHeader } from 'feature/addressConfirmation/components/ConfirmHeader';
import { DescriptionInput } from 'feature/addressConfirmation/components/Styled/DescriptionInput';
import { BlueButton } from 'feature/addressConfirmation/components/Styled/BlueButton';
import { postAddress } from 'feature/addressConfirmation/helpers/postAddress';
import { useSnackbar } from 'notistack';
import { useRouter } from 'next/router';

export default function PlaceId({ apiKey, details }) {
  const { enqueueSnackbar } = useSnackbar();
  const router = useRouter();
  const [adrComplement, setAdrComplement] = useState('');
  if (!details) {
    return (
      <Typography style={{ marginTop: '24px' }} variant="h6" align="center">
        Id de dirección no válida
      </Typography>
    );
  }
  const editAddress = () =>
    window.location.assign(
      `/?initial_query=${encodeURIComponent(
        `${details.route} #${details.streetInfo.internal}${
          details.streetInfo.external ? `-${details.streetInfo.external}` : ''
        }`,
      )}&coords=${details.latitude},${details.longitude}`,
    );
  const doPostAddress = () => {
    const { placeId } = details;
    postAddress({ placeId, dirComplement: adrComplement })
      .then(() => {
        enqueueSnackbar('Dirección almacenada correctamente', {
          variant: 'success',
        });
        router.push('/');
      })
      .catch(() => {
        enqueueSnackbar('Ocurrió un error al enviar la Dirección', {
          variant: 'error',
        });
      });
  };
  return (
    <>
      <ConfirmHeader />
      <MapMarker
        apiKey={apiKey}
        latitude={details.latitude}
        longitude={details.longitude}
      />
      <div
        style={{
          position: 'absolute',
          width: '100%',
          zIndex: 1,
          background: '#FFFFFF',
          boxShadow: '0px -3px 4px rgba(0, 0, 0, 0.25)',
          borderRadius: '10px 10px 0px 0px',
          height: '100px',
          padding: '16px 16px 0 16px',
          marginTop: '220px',
        }}
      >
        <Typography
          style={{
            fontFamily: 'Rawline Bold',
            letterSpacing: '0.005em',
            color: '#767676',
          }}
          variant="h6"
        >
          Dirección
        </Typography>
        <div
          style={{
            display: 'flex',
            marginTop: '8px',
            paddingBottom: '12px',
            alignItems: 'center',
            borderBottom: '1px solid #B9B9B9',
          }}
        >
          <div style={{ display: 'flex', flexGrow: 1 }}>
            <Typography
              style={{
                fontFamily: 'Rawline Bold',
                letterSpacing: '0.005em',
              }}
              variant="h5"
            >
              {details.route}
            </Typography>
            {details.streetInfo.internal && (
              <>
                <Typography
                  style={{
                    marginLeft: '8px',
                    fontFamily: 'Rawline Bold',
                    letterSpacing: '0.005em',
                  }}
                  variant="h5"
                >
                  #
                </Typography>
                <Typography
                  style={{
                    marginLeft: '8px',
                    fontFamily: 'Rawline Bold',
                    letterSpacing: '0.005em',
                  }}
                  variant="h5"
                >
                  {details.streetInfo.internal}
                </Typography>
              </>
            )}
            {details.streetInfo.external && (
              <>
                <Typography
                  style={{
                    marginLeft: '8px',
                    fontFamily: 'Rawline Bold',
                    letterSpacing: '0.005em',
                  }}
                  variant="h5"
                >
                  -
                </Typography>
                <Typography
                  style={{
                    marginLeft: '8px',
                    fontFamily: 'Rawline Bold',
                    letterSpacing: '0.005em',
                  }}
                  variant="h5"
                >
                  {details.streetInfo.external}
                </Typography>
              </>
            )}
          </div>
          <IconButton onClick={editAddress}>
            <MdCreate style={{ color: '#8E8E8E' }} size={32} />
          </IconButton>
        </div>
        <Typography
          style={{
            marginTop: '32px',
            fontFamily: 'Rawline Bold',
            letterSpacing: '0.005em',
            color: '#767676',
          }}
          variant="h6"
        >
          Apto, piso, casa
        </Typography>
        <DescriptionInput
          placeholder="Agrega un complemento a tu dirección como piso o apartamento"
          id="filled-multiline-flexible"
          multiline
          value={adrComplement}
          onChange={(evt) => setAdrComplement(evt.target.value)}
          rowsMax={3}
          rows={3}
        />
        <BlueButton
          style={{ marginTop: '24px', marginBottom: '16px' }}
          fullWidth
          size="large"
          onClick={doPostAddress}
        >
          Confirmar
        </BlueButton>
      </div>
    </>
  );
}

export const getServerSideProps = async (ctx) => {
  const { placeId } = ctx.query;
  const details = await getPlaceDetails(String(placeId));
  return { props: { apiKey: process.env.GOOGLE_MAPS_API_KEY, details } };
};
