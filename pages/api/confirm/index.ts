import { NextApiRequest, NextApiResponse } from 'next';

export default async (req: NextApiRequest, res: NextApiResponse) => {
  if (req.method !== 'POST') {
    res.status(400).send({ msg: 'Bad request' });
    return;
  }
  const { placeId } = req.body;
  if (!placeId) {
    res.status(400).send({ msg: 'Bad request' });
    return;
  }
  res.status(201).send({ msg: 'Created' });
};
