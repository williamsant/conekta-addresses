import { NextApiRequest, NextApiResponse } from 'next';
import { getAddressFromLocation } from 'services/search';

export default async (req: NextApiRequest, res: NextApiResponse) => {
  if (req.method !== 'GET') {
    res.status(400).send({ msg: 'Bad request' });
    return;
  }

  const { coords } = req.query;
  if (!coords) {
    res.status(400).send({ msg: 'Bad request' });
    return;
  }
  try {
    const placeData = await getAddressFromLocation(String(coords));
    res.status(200).send(placeData);
  } catch (err) {
    res.status(500).send(err);
  }
};
